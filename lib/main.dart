import 'package:db_poc/add_transaction_screen.dart';
import 'package:db_poc/database_manager.dart';
import 'package:db_poc/transaction_model.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DatabaseManager().init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'DB Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: FutureBuilder(
        future: DatabaseManager().getAllFinancialTransactions(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          } else if (snapshot.hasData) {
            if (snapshot.data != null) {
              return ListView.builder(
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: ListTile(
                    tileColor: Theme.of(context)
                        .colorScheme
                        .inversePrimary
                        .withOpacity(0.5),
                    title: Text("# ${snapshot.data![index].id}"),
                    subtitle: Text(snapshot.data![index].receiver),
                    trailing: IconButton(
                      onPressed: () {
                        _showDeleteTransactionDialog(
                            context, snapshot.data![index], index);
                      },
                      icon: const Icon(Icons.delete),
                    ),
                    leading: IconButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (_) => TransactionScreen(
                              transaction: snapshot.data![index],
                            ),
                          ),
                        );
                      },
                      icon: const Icon(Icons.edit),
                    ),
                    // Row(
                    //   children: [
                    //     Text("${snapshot.data![index].amount} JOD"),
                    //
                    //   ],
                    // ),
                  ),
                ),
                itemCount: snapshot.data!.length,
              );
            }
            return const Center(
              child: Text('There are no transactions yet'),
            );
          }
          return const SizedBox.shrink();
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (_) => const TransactionScreen()));
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  void _showDeleteTransactionDialog(
      BuildContext context, FinancialTransaction transaction, int index) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title:
                const Text('Are you sure you want to delete this transaction?'),
            actions: [
              ElevatedButton(
                onPressed: () async {
                  await DatabaseManager()
                      .deleteFinancialTransaction(transaction);
                  Navigator.pop(context);
                  setState(() {});
                },
                child: const Text('Yes'),
              ),
              ElevatedButton(
                onPressed: () => Navigator.pop(context),
                child: const Text('No'),
              ),
            ],
          );
        });
  }
}

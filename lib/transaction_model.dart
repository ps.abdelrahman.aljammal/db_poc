class FinancialTransaction {
  final int id;
  final String receiver;
  final double amount;

  const FinancialTransaction({
    required this.id,
    required this.receiver,
    required this.amount,
  });

  factory FinancialTransaction.fromMap(Map<String, dynamic> map) {
    return FinancialTransaction(
      id: map['id'],
      receiver: map['receiver'],
      amount: map['amount']/1.0,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'receiver': receiver,
      'amount': amount,
    };
  }

  @override
  String toString() {
    return 'FinancialTransaction{id: $id, receiver: $receiver, amount: $amount}';
  }
}

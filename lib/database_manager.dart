import 'package:db_poc/transaction_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseManager {
  static final DatabaseManager shared = DatabaseManager._private();

  factory DatabaseManager() => shared;

  DatabaseManager._private();

  late Database database;

  Future<void> init() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'demo.db');
    database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE transactions (id INTEGER PRIMARY KEY, receiver TEXT, amount INTEGER)');
    });
  }

  Future<void> insertFinancialTransaction(
      FinancialTransaction transaction) async {
    await database.rawInsert(
        'INSERT INTO transactions(id, receiver, amount) VALUES(?, ?, ?)',
        [transaction.id, transaction.receiver, transaction.amount]);
  }

  Future<void> updateFinancialTransaction(
      FinancialTransaction transaction) async {
    await database.rawUpdate(
        'UPDATE transactions SET receiver = ?, amount = ? WHERE id = ?',
        [transaction.receiver, transaction.amount, transaction.id]);
  }

  Future<void> deleteFinancialTransaction(
      FinancialTransaction transaction) async {
    await database
        .rawDelete('DELETE FROM transactions WHERE id = ?', [transaction.id]);
  }

  Future<List<FinancialTransaction>> getAllFinancialTransactions() async {
    final List<Map<String, dynamic>> maps =
        await database.rawQuery('SELECT * FROM transactions');
    if (maps.isEmpty) {
      return [];
    }
    return List.generate(
        maps.length, (index) => FinancialTransaction.fromMap(maps[index]));
  }

  Future<int> getLastTransactionId() async {
    var res =
        await database.rawQuery('SELECT MAX(id) as max_id FROM transactions');
    return int.tryParse(res[0]['max_id'].toString()) ?? 0;
  }

  Future<void> close() async {
    await database.close();
  }
}

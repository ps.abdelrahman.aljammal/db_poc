import 'package:db_poc/database_manager.dart';
import 'package:db_poc/main.dart';
import 'package:db_poc/transaction_model.dart';
import 'package:flutter/material.dart';

class TransactionScreen extends StatefulWidget {
  const TransactionScreen({super.key, this.transaction});

  final FinancialTransaction? transaction;

  @override
  State<TransactionScreen> createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  bool _isUpdateTransaction = false;
  TextEditingController receiverController = TextEditingController();
  TextEditingController amountController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _isUpdateTransaction = widget.transaction != null;
    if (_isUpdateTransaction) {
      receiverController.text = widget.transaction!.receiver;
      amountController.text = widget.transaction!.amount.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(
            _isUpdateTransaction ? "Update Transaction" : "Add Transaction"),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: receiverController,
                decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.person,
                      color: Theme.of(context).primaryColor,
                    ),
                    label: const Text("Receiver"),
                    alignLabelWithHint: false,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: amountController,
                decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.monetization_on,
                      color: Theme.of(context).primaryColor,
                    ),
                    label: const Text("Amount"),
                    alignLabelWithHint: false,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                onPressed: () async {
                  String receiver = receiverController.text;
                  double amount = double.tryParse(amountController.text) ?? 0;
                  if (amount != 0 && receiver.isNotEmpty) {
                    _isUpdateTransaction
                        ? await DatabaseManager()
                            .updateFinancialTransaction(FinancialTransaction(
                            id: widget.transaction!.id,
                            receiver: receiver,
                            amount: amount,
                          ))
                        : await DatabaseManager()
                            .insertFinancialTransaction(FinancialTransaction(
                            id: await DatabaseManager().getLastTransactionId() +
                                1,
                            receiver: receiver,
                            amount: amount,
                          ));
                    setState(() {});
                    Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (_) =>
                              const MyHomePage(title: 'DB Demo Home Page')),
                      (route) => false,
                    );
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    _isUpdateTransaction ? "Update" : "Add",
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
